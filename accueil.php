<?php 
session_start();
include 'core/include.php';
if (!empty ($_SESSION['user']) )
    {
    $user = fixObject($_SESSION['user']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php    include 'page_slices/head.php'; ?>
<body>
<div id="maxi_conteneur">
    <?php
        include 'page_slices/banniere.php';
    ?>
    <div id="conteneur">
        <?php
    include 'page_slices/menuh.php';
        ?>
            <br />
            <?php
            $now = getdate();
            $nb_Jours_du_mois = date("t", mktime(0, 0, 0, $now['mon'], 1, $now['year'])); 
            $beginPeriod = $now['year'] . '-' . $now['mon'] . '-01 00:00:00';
            $endPeriod = $now['year'] . '-' . $now['mon'] . '-' . $nb_Jours_du_mois . ' 23:59:59' ;
            
            echo'<center>Bonjour '.$user->getFull_name().'!<br>';
            echo '<br />Total des incidents: '.Incident::countAll();
            
            echo '<br />';
            echo '<br />Total des actions correctives: '.CorrectiveAction::countAll();
           
            echo '</center>';
            ?>
            <br />
            <div id="error_contener" style="text-align: center">
                
            </div>
    </div>
    
        <?php
            include 'page_slices/bas_contenu.php';
         ?>
    </div>
    
</body>
</html>
<?php
    }	
else
{
    header('Location: index.php');
}
?>